# Retrieve BlockStack Private Key from passphrase

This script allows to extract the private key generated for your identity on BlockStack.
It requires the passphrase you've used when creating the identity.

## Required
1. git
2. npm
3. python2.7
4. jq

## Run
Usage:  
`./get-blockstack-private-key.sh "passphrase words quoted" output_directory`

So, if your passphrase is _"run forest run, I tells ya"_, then run the script like this:  
`./get-blockstack-private-key.sh "run forest run, I tells ya" ~/.ssh`
