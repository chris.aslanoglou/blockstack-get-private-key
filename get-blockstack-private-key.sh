#!/bin/bash
# Retrieve BlockStack private key from a passphrase.
# Requires:
# git, npm, python2.7, jq

PRIVATE_KEY_FILENAME='bns_privatekey.hex'

# Passphrase can consist of multiple words.
if [ $# -ne 2 ]; then
	echo "Usage: $(basename $0) \"passphrase\" output_directory"
	exit 0
fi
PASSPHRASE=$1
OUTPUT_DIR=$2

echo "Cloning cli-blockstack..."
git clone --quiet https://github.com/blockstack/cli-blockstack
cd cli-blockstack
# Restore to previous commit where build was passing (this is the commit from when I
# had originally set up the Python Notebook).
git reset --quiet --hard ea44a2d2e155362c2e70fdd3cc4df0d1efb13d86
printf "Installing cli-blockstack... (this might take a few minutes ~5)\n\n"
npm --silent install --no-progress 2> /dev/null
printf "\nLinking cli-blockstack...\n"
npm --silent link
printf "\nStoring BNS private key in ${OUTPUT_DIR}/${PRIVATE_KEY_FILENAME}\n\n"
blockstack-cli get_owner_keys "${PASSPHRASE}" | jq -r '.[0].privateKey' > ${OUTPUT_DIR}/${PRIVATE_KEY_FILENAME}

cd ..
rm -rf ./cli-blockstack
echo "Done!"